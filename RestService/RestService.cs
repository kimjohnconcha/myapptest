﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Rest.Http
{
    public class RestService : IRestService, IDisposable
    {
        private HttpClient _client;
        /// <summary>
        /// Gets or sets the HttpCllient.
        /// </summary>
        /// <value>The client.</value>
        public HttpClient Client
        {
            get { return _client; }
            set { _client = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestService.RestService"/> class.
        /// </summary>
        public RestService()
        {
            _client = new HttpClient();
            InitializeClient();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestService.RestService"/> class.
        /// </summary>
        /// <param name="url">Base URI of the API.</param>
        public RestService(string url)
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(url);
            InitializeClient();
        }

        /// <summary>
        /// Sets up the necessary client details, eg. header defaults.
        /// </summary>
        private void InitializeClient()
        {
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            _client.Timeout = TimeSpan.FromSeconds(20);
        }

        public void SetTimeout(TimeSpan ts)
        {
            _client.Timeout = ts;
        }

        /// <summary>
        /// Asynchronously performs an HTTP POST request.
        /// </summary>
        /// <param name="uri">Create URI.</param>
        /// <param name="json">Serialized JSON object.</param>
        /// <typeparam name="T">IRestResponse.</typeparam>
        public async Task<T> Post<T>(string uri, string json) where T : IRestResponse
        {
            StringContent body = new StringContent(json, Encoding.UTF8, "application/json");
            IRestResponse instance = ((IRestResponse)Activator.CreateInstance<T>());
            CancellationTokenSource source = new CancellationTokenSource();

            try
            {
                HttpResponseMessage response = await _client.PostAsync(uri, body, source.Token);
                instance.Serialize(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                instance.HandleException(e, source);

            }
            return (T)instance;
        }

        /// <summary>
        /// Asynchronously performs an HTTP GET request.
        /// </summary>
        /// <param name="parameters">Fetch URI.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public async Task<T> Get<T>(string parameters) where T : IRestResponse
        {
            IRestResponse instance = ((IRestResponse)Activator.CreateInstance<T>());
            CancellationTokenSource source = new CancellationTokenSource();

            try
            {
                DateTime start = DateTime.Now;
                HttpResponseMessage response = await _client.GetAsync(parameters, source.Token);
                instance.Serialize(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error Rest Service Get Request: " + e.Message);
                instance.HandleException(e, source);
            }

            return (T)instance;
        }

        /// <summary>
        /// Asynchronously performs an HTTP PUT request.
        /// </summary>
        /// <param name="uri">Update URI.</param>
        /// <param name="json">Serialized JSON object.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public async Task<T> Put<T>(string uri, string json) where T : IRestResponse
        {
            StringContent body = new StringContent(json, Encoding.UTF8, "application/json");
            IRestResponse instance = ((IRestResponse)Activator.CreateInstance<T>());
            CancellationTokenSource source = new CancellationTokenSource();

            try
            {
                HttpResponseMessage response = await _client.PutAsync(uri, body, source.Token);
                instance.Serialize(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                instance.HandleException(e, source);
            }

            return (T)instance;
        }

        /// <summary>
        /// Asynchronously performs an HTTP DELETE request.
        /// </summary>
        /// <param name="parameters">Destroy URI.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public async Task<T> Delete<T>(string parameters) where T : IRestResponse
        {
            IRestResponse instance = ((IRestResponse)Activator.CreateInstance<T>());
            CancellationTokenSource source = new CancellationTokenSource();

            try
            {
                HttpResponseMessage response = await _client.DeleteAsync(parameters, source.Token);
                instance.Serialize(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                instance.HandleException(e, source);
            }

            return (T)instance;
        }

        public async Task<T> FormUpload<T>(string uri, Dictionary<string, object> parameters) where T : IRestResponse
        {
            IRestResponse instance = ((IRestResponse)Activator.CreateInstance<T>());
            CancellationTokenSource source = new CancellationTokenSource();

            try
            {
                MultipartFormDataContent content = new MultipartFormDataContent();

                foreach (KeyValuePair<string, object> p in parameters)
                {
                    if (p.Value.GetType() == typeof(string))
                    {
                        StringContent s = new StringContent((string)p.Value);
                        s.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                        {
                            Name = "\"" + p.Key + "\""
                        };

                        content.Add(s);
                    }
                    else
                    {
                        ByteArrayContent s = new ByteArrayContent((byte[])p.Value);
                        s.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                        {
                            Name = "\"" + p.Key + "\"",
                            FileName = "photo.jpg"
                        };
                        content.Add(s);
                    }
                }

                HttpResponseMessage response = await _client.PostAsync(uri, content, source.Token);
                instance.Serialize(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                instance.HandleException(e, source);
            }

            return (T)instance;
        }

        public async Task<T> VideoUpload<T>(string uri, Dictionary<string, object> parameters) where T : IRestResponse
        {
            IRestResponse instance = ((IRestResponse)Activator.CreateInstance<T>());
            CancellationTokenSource source = new CancellationTokenSource();

            try
            {
                MultipartFormDataContent content = new MultipartFormDataContent();

                foreach (KeyValuePair<string, object> p in parameters)
                {
                    if (p.Value.GetType() == typeof(string))
                    {
                        StringContent s = new StringContent((string)p.Value);
                        s.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                        {
                            Name = "\"" + p.Key + "\""
                        };

                        content.Add(s);
                    }
                    else if (p.Value.GetType() == typeof(MediaContent))
                    {
                        MediaContent file = (MediaContent)p.Value;
                        StreamContent s = new StreamContent(file.Stream);

                        s.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                        {
                            Name = "\"" + p.Key + "\"",
                            FileName = file.Filename
                        };
                        content.Add(s);
                    }
                }

                HttpResponseMessage response = await _client.PostAsync(uri, content, source.Token);
                instance.Serialize(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                instance.HandleException(e, source);
            }

            return (T)instance;
        }

        public async Task<T> FormUpdate<T>(string uri, Dictionary<string, object> parameters) where T : IRestResponse
        {
            IRestResponse instance = ((IRestResponse)Activator.CreateInstance<T>());
            CancellationTokenSource source = new CancellationTokenSource();

            try
            {
                MultipartFormDataContent content = new MultipartFormDataContent();

                foreach (KeyValuePair<string, object> p in parameters)
                {

                    if (p.Value.GetType() == typeof(string))
                    {
                        StringContent s = new StringContent((string)p.Value);
                        s.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                        {
                            Name = "\"" + p.Key + "\""
                        };

                        content.Add(s);
                    }
                    else
                    {

                        ByteArrayContent s = new ByteArrayContent((byte[])p.Value);
                        s.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                        {
                            Name = "\"" + p.Key + "\"",
                            FileName = "photo.jpg"
                        };

                        content.Add(s);
                    }

                }

                HttpResponseMessage response = await _client.PutAsync(uri, content, source.Token);
                instance.Serialize(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                instance.HandleException(e, source);
            }

            return (T)instance;
        }

        public void Dispose()
        {
            if (_client != null)
            {
                _client.Dispose();
                _client = null;
            }
        }
    }
}
