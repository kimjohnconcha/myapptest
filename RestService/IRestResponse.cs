﻿using System;
using System.Threading;

namespace Rest.Http
{
    public interface IRestResponse
    {
        string Errors { get; }
        bool Success { get; }

        /// <summary>
        /// Serialize the specified content.
        /// </summary>
        /// <param name="content">Content.</param>
        void Serialize(string content);

        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="e">E.</param>
        /// <param name="source">Source.</param>
        void HandleException(Exception e, CancellationTokenSource source);
    }
}
