﻿using System;
using System.IO;

namespace Rest.Http
{
    public class MediaContent
    {
        public string Filename { get; set; }
        public string MediaType { get; set; }
        public Stream Stream { get; set; }
    }
}
