﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Rest.Http
{
    public interface IRestService
    {
        /// <summary>
        /// Post the specified uri and json.
        /// </summary>
        /// <param name="uri">URI.</param>
        /// <param name="json">Json.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        Task<T> Post<T>(string uri, string json) where T : IRestResponse;
        /// <summary>
        /// Get the specified parameters.
        /// </summary>
        /// <param name="parameters">Parameters.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        Task<T> Get<T>(string parameters) where T : IRestResponse;
        /// <summary>
        /// Put the specified uri and json.
        /// </summary>
        /// <param name="uri">URI.</param>
        /// <param name="json">Json.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        Task<T> Put<T>(string uri, string json) where T : IRestResponse;
        /// <summary>
        /// Delete the specified parameters.
        /// </summary>
        /// <param name="parameters">Parameters.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        Task<T> Delete<T>(string parameters) where T : IRestResponse;

        Task<T> FormUpload<T>(string uri, Dictionary<string, object> parameters) where T : IRestResponse;

        Task<T> VideoUpload<T>(string uri, Dictionary<string, object> parameters) where T : IRestResponse;

        Task<T> FormUpdate<T>(string uri, Dictionary<string, object> parameters) where T : IRestResponse;
    }
}
