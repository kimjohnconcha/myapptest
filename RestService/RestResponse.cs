﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Rest.Http
{
    public class RestResponse : IRestResponse
    {
        public string Errors
        {
            get; protected set;
        }

        public bool Success
        {
            get; protected set;
        }

        public virtual void HandleException(Exception e, CancellationTokenSource source)
        {
            System.Diagnostics.Debug.WriteLine("HandleException: {0}", e.Message);
            Type exceptionType = e.GetType();
            if (exceptionType == typeof(TaskCanceledException))
            {
                TaskCanceledException tce = (TaskCanceledException)e;
                if (tce.CancellationToken == source.Token)
                {
                    Errors = "Task canceled by the user.";
                }
                else
                {
                    Errors = "Web request timed out.";
                }
            }
            else if (exceptionType == typeof(WebException))
            {
                System.Net.WebException ex = (System.Net.WebException)e;
                var response = (HttpWebResponse)ex.Response;

                if (response != null)
                {
                    var dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    var serverMessage = reader.ReadToEnd();

                    Dictionary<string, string> status = JsonConvert.DeserializeObject<Dictionary<string, string>>(serverMessage);
                    if (status != null)
                    {
                        if (status.ContainsKey("error"))
                        {
                            Errors = status["error"];
                        }
                        else if (status.ContainsKey("errors"))
                        {
                            Errors = status["errors"];
                        }
                        else if (status.ContainsKey("message"))
                        {
                            Errors = status["message"];
                        }
                        else
                        {
                            Errors = "Unknown error";
                        }
                    }
                    else
                    {
                        Errors = "Unknown error";
                    }
                }
                else
                {
                    Errors = "There was an error connecting to the server.";
                }

            }
            else
            {
                System.Diagnostics.Debug.WriteLine("{0}", e.Message);
            }

            Success = false;
        }

        public virtual void Serialize(string content)
        {
            throw new NotImplementedException();
        }
    }
}
