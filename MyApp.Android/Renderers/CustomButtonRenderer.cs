﻿using System;
using Android.Content;
using MyApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Button), typeof(CustomButtonRenderer))]
namespace MyApp.Droid.Renderers
{
    public class CustomButtonRenderer : ButtonRenderer
    {
        public CustomButtonRenderer(Context context) : base(context) { }


        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                this.Control.SetPadding(0, 0, 0, 5);
                Control.TextAlignment = Android.Views.TextAlignment.Center;
                this.Control.TextSize = (float)e.NewElement.FontSize - 2;
            }
        }

    }
}
