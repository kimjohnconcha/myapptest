﻿using System;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using MyApp.Controls;
using MyApp.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;


[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer_droid))]
namespace MyApp.Droid.Renderers
{
    public class CustomEntryRenderer_droid : EntryRenderer
    {
        public CustomEntryRenderer_droid(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control == null || e.NewElement == null) return;

            Control.SetTextColor(Android.Graphics.Color.Blue);


        }
    }
}
