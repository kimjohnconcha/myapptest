﻿using System;
using Xamarin.Forms;

namespace MyApp.Controls
{
    public class NoBorderEntry : Entry
    {
        public void InvalidateLayout()
        {
            InvalidateMeasure();
        }

        public static readonly BindableProperty ReturnKeyProperty = BindableProperty.Create("ReturnKey", typeof(string), typeof(CustomEntry), "Return");

        public string ReturnKey
        {
            get { return (string)GetValue(ReturnKeyProperty); }
            set { SetValue(ReturnKeyProperty, value); }
        }

        public static readonly BindableProperty AutoCapitalizationProperty = BindableProperty.Create("AutoCapitalization", typeof(string), typeof(CustomEntry), "None");

        public string AutoCapitalization
        {
            get { return (string)GetValue(AutoCapitalizationProperty); }
            set { SetValue(AutoCapitalizationProperty, value); }
        }

        public static readonly BindableProperty ButtonModeProperty = BindableProperty.Create("ButtonMode", typeof(bool), typeof(CustomEntry), true);

        public bool ButtonMode
        {
            get { return (bool)GetValue(ButtonModeProperty); }
            set { SetValue(ButtonModeProperty, value); }
        }
        /// <summary>
        /// Text alignment values:
        /// 0 - left
        /// 1 - right
        /// 2 - center
        /// 3 - justified
        /// </summary>
        public static readonly BindableProperty TextAlignmentProperty = BindableProperty.Create("TextAlignment", typeof(int), typeof(CustomEntry), 0);

        public int TextAlignment
        {
            get { return (int)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public Entry NextEntry { get; set; }

        public static readonly BindableProperty PlaceholderOriginalColorProperty = BindableProperty.Create("PlaceholderOriginalColor", typeof(Color), typeof(CustomEntry), Color.White);

        public Color PlaceholderOriginalColor
        {
            get { return (Color)GetValue(PlaceholderOriginalColorProperty); }
            set { SetValue(PlaceholderOriginalColorProperty, value); }
        }

        public static readonly BindableProperty MoveUpProperty = BindableProperty.Create("MoveUp", typeof(bool), typeof(CustomEntry), true);

        public bool MoveUp
        {
            get { return (bool)GetValue(MoveUpProperty); }
            set { SetValue(MoveUpProperty, value); }
        }

        public bool hasBorder { get; set; }
        public Color BorderColor { get; set; }
        public float BorderSize { get; set; }
    }
}
