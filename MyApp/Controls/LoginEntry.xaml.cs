﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace MyApp.Controls
{
    public partial class LoginEntry : ContentView
    {

        public static readonly BindableProperty EntryTypeProperty =
            BindableProperty.Create(nameof(EntryType), typeof(string), typeof(LoginEntry), "", BindingMode.TwoWay,
        propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as LoginEntry;
            //System.Diagnostics.Debug.WriteLine((string)newValue);
            //view.entry.Text = (string)newValue;

            //var _type = (string)newValue;

            //switch(_type)
            //{
            //    case "Username":
            //        view.entry.SetBinding(Entry.TextProperty, "Username");
            //        break;

            //    case "Email":
            //        view.entry.SetBinding(Entry.TextProperty, "Email");
            //        break;
            //}


        });

        public string EntryType
        {
            get { return (string)GetValue(EntryTypeProperty); }
            set { SetValue(EntryTypeProperty, value); }
        }

        public LoginEntry()
        {
            InitializeComponent();
        }
    }
}
