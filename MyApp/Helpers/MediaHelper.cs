﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;

namespace MyApp.Helpers
{
    public static class MediaHelper
    {
        public static byte[] ToByteArray(this Stream stream)
        {
            MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);

            return ms.ToArray();
        }

        static async System.Threading.Tasks.Task<bool> RequestMediaPermission()
        {
            var tcs = new System.Threading.Tasks.TaskCompletionSource<bool>();

            try
            {
                // check if all necessary permissions have been granted
                var cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Camera);
                var storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Storage);

                System.Diagnostics.Debug.WriteLine("Camera: {0}\tStorage: {1}", cameraStatus, storageStatus);

                // ask for permission if there's a permission that wasn't granted
                if (!AllPermissionsGranted(new Plugin.Permissions.Abstractions.PermissionStatus[] { cameraStatus, storageStatus }))
                {
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Plugin.Permissions.Abstractions.Permission.Camera, Plugin.Permissions.Abstractions.Permission.Storage });
                    cameraStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Camera);
                    storageStatus = await CrossPermissions.Current.CheckPermissionStatusAsync(Plugin.Permissions.Abstractions.Permission.Storage);
                }

                // check again, and if granted, show preview
                if (AllPermissionsGranted(new Plugin.Permissions.Abstractions.PermissionStatus[] { cameraStatus, storageStatus }))
                {
                    tcs.SetResult(true);
                }
                else
                {
                    tcs.SetResult(false);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Could not get permission: " + ex.Message);
                tcs.SetResult(false);
            }

            return await tcs.Task;
        }


        static bool AllPermissionsGranted(Plugin.Permissions.Abstractions.PermissionStatus[] statuses)
        {
            foreach (var status in statuses)
            {
                System.Diagnostics.Debug.WriteLine("{0}", status);
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    return false;
                }
            }
            return true;
        }


        public static async Task<MediaFile> SelectPhotoAsync()
        {
            await CrossMedia.Current.Initialize();

            // request permissions
            bool permissionsGranted = await RequestMediaPermission();
            System.Diagnostics.Debug.WriteLine("Permissions granted: {0}", permissionsGranted);
            if (!permissionsGranted)
            {
                return null;
            }

            var options = new List<string>();

            if (!CrossMedia.Current.IsPickPhotoSupported && !CrossMedia.Current.IsTakePhotoSupported)
            {
                throw new Exception("Cannot choose photo");
            }

            if (CrossMedia.Current.IsPickPhotoSupported)
            {
                options.Add("Choose From Gallery");
            }

            if (CrossMedia.Current.IsTakePhotoSupported && CrossMedia.Current.IsCameraAvailable)
            {
                options.Add("Take Photo");
            }

            var choice = await UserDialogs.Instance.ActionSheetAsync("Upload a photo", "No Thanks", null, null, options.ToArray());

            MediaFile file = null;

            if (choice == "Choose From Gallery")
            {
                file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                {
                    PhotoSize = PhotoSize.Medium,
                    CompressionQuality = 92,
                });
            }
            else if (choice == "Take Photo")
            {
                file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions()
                {
                    Directory = "MyApp",
                    Name = string.Format("{0}.jpg", DateTime.Now.ToString("s")),
                    SaveToAlbum = true,
                    PhotoSize = PhotoSize.Medium,
                    CompressionQuality = 92,
                    AllowCropping = false,
                    RotateImage = false,
                });
            }
            else
            {
                file = null;
            }

            return file;
        }

    }
}
