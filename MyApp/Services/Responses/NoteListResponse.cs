﻿using System;
using System.Collections.Generic;
using MyApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rest.Http;

namespace MyApp
{
    public class NoteListResponse : RestResponse
    {
        public List<Note> Notes
        {
            get; private set;
        }

        protected JObject _parsed;
        public override void Serialize(string content)
        {
            _parsed = JObject.Parse(content);
            JToken statusToken;

            if ((statusToken = _parsed.SelectToken("status")) != null)
            {
                if (statusToken.ToString() == "200")
                {
                    Notes = JsonConvert.DeserializeObject<List<Note>>(_parsed["notes"].ToString());
                    Success = true;
                }
                else
                {
                    try
                    {
                        Errors = _parsed["errors"].ToString();
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine("Error: " + e.Message);
                        Errors = _parsed["error"].ToString();
                    }
                    Success = false;
                }
            }
            else
            {
                try
                {
                    Errors = _parsed["errors"].ToString();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Error: " + e.Message);
                    Errors = _parsed["error"].ToString();
                }
                Success = false;
            }
        }
    }
}
