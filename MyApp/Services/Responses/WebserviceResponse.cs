﻿using System;
using Newtonsoft.Json.Linq;
using Rest.Http;

namespace MyApp
{
    public class WebserviceResponse : RestResponse
    {
        public string Message
        {
            get; private set;
        }

        protected JObject _parsed;
        public override void Serialize(string content)
        {
            _parsed = JObject.Parse(content);
            JToken statusToken;

            if ((statusToken = _parsed.SelectToken("status")) != null)
            {
                if (statusToken.ToString() == "200")
                {
                    Message = _parsed["message"].ToString();
                    Success = true;
                }
                else
                {
                    try
                    {
                        Errors = _parsed["error"].ToString();
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e.Message);
                        Errors = _parsed["message"].ToString();
                    }
                    Success = false;
                }
            }
        }
    }
}
