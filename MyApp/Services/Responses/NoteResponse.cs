﻿using System;
using MyApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rest.Http;

namespace MyApp
{
    public class NoteResponse : RestResponse
    {
        public Note Note
        {
            get; private set;
        }

        protected JObject _parsed;
        public override void Serialize(string content)
        {
            _parsed = JObject.Parse(content);
            JToken statusToken;

            System.Diagnostics.Debug.WriteLine(_parsed);

            if ((statusToken = _parsed.SelectToken("status")) != null)
            {
                if (statusToken.ToString() == "200")
                {
                    Note = JsonConvert.DeserializeObject<Note>(_parsed["note"].ToString());
                    Success = true;
                }
                else
                {
                    try
                    {
                        Errors = _parsed["errors"].ToString();
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine("Error: " + e.Message);
                        Errors = _parsed["error"].ToString();
                    }
                    Success = false;
                }
            }
            else
            {
                try
                {
                    Errors = _parsed["errors"].ToString();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Error: " + e.Message);
                    Errors = _parsed["error"].ToString();
                }
                Success = false;
            }
        }
    }
}
