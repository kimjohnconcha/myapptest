﻿using System;
using MyApp.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rest.Http;

namespace MyApp
{
    public class AuthenticationResponse : RestResponse
    {
        public User User
        {
            get; private set;
        }

        protected JObject _parsed;
        public override void Serialize(string content)
        {
            _parsed = JObject.Parse(content);
            JToken statusToken;

            if ((statusToken = _parsed.SelectToken("status")) != null)
            {
                if (statusToken.ToString() == "200")
                {
                    User = JsonConvert.DeserializeObject<User>(_parsed["user"].ToString());
                    Success = true;
                }
                else
                {
                    try
                    {
                        Errors = _parsed["errors"].ToString();
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine("Error: " + e.Message);
                        Errors = _parsed["error"].ToString();
                    }
                    Success = false;
                }
            }
            else
            {
                try
                {
                    Errors = _parsed["errors"].ToString();
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("Error: " + e.Message);
                    Errors = _parsed["error"].ToString();
                }
                Success = false;
            }
        }

    }
}
