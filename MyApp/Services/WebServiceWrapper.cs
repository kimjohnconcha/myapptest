﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyApp.Models;
using Newtonsoft.Json;
using Rest.Http;

namespace MyApp.Services
{
    public class WebServiceWrapper
    {
        private RestService _client;

        public WebServiceWrapper()
        {
            _client = new RestService(Api.RootUrl);
        }

        public async Task<AuthenticationResponse> LoginAsync(string email, string password)
        {
            var request = new
            {
                user = new
                {
                    email = email,
                    password = password,
                }
            };

            string json = JsonConvert.SerializeObject(request, Formatting.Indented);
            System.Diagnostics.Debug.WriteLine("Sign-in URL : " + Api.Signin + "\nJSN: " + json);
            AuthenticationResponse response = await _client.Post<AuthenticationResponse>(Api.Signin, json);
            return response;
        }


        public async Task<AuthenticationResponse> RegisterAsync(User u, string password)
        {
            var request = new
            {
                user = new
                {
                    email = u.Email,
                    username = u.Username,
                    password = password
                }
            };

            string json = JsonConvert.SerializeObject(request, Formatting.Indented);
            System.Diagnostics.Debug.WriteLine("Sign-up URL : " + Api.Register + "\nJSN: " + json);
            AuthenticationResponse response = await _client.Post<AuthenticationResponse>(Api.Register, json);
            return response;
        }

        public async Task<NoteListResponse> GetNotes(string token)
        {
            string url = string.Format("{0}?token={1}", Api.Note, CurrentSession.CurrentUser.AccessToken);
            System.Diagnostics.Debug.WriteLine("get note URL : " + url);
            NoteListResponse response = await _client.Get<NoteListResponse>(url);
            return response;
        }

        public async Task<NoteResponse> CreateNote(Note n, byte[] image)
        {
            var client = new RestService();
            client.Client.BaseAddress = new Uri(Api.RootUrl);
            client.SetTimeout(TimeSpan.FromMinutes(20));
            client.Client.DefaultRequestHeaders.Accept.Clear();
            client.Client.MaxResponseContentBufferSize = 300000;

            Dictionary<string, object> req = new Dictionary<string, object>();

            req.Add("note[title]", n.Title);
            req.Add("note[description]", n.Description);

            if (image != null)
                req.Add("note[image]", image);

            if(n.ID != 0)
                req.Add("note[id]", n.ID.ToString());


            string url = string.Format("{0}?token={1}", Api.Note, CurrentSession.CurrentUser.AccessToken);
            System.Diagnostics.Debug.WriteLine("create note : " + url + "\nREQ: " + req);
            NoteResponse response = await client.FormUpload<NoteResponse>(url, req);
            return response;
        }

        //public async Task<NoteResponse> UpdateNote(Note n, byte[] image)
        //{
        //    var client = new RestService();
        //    client.Client.BaseAddress = new Uri(Api.RootUrl);
        //    client.SetTimeout(TimeSpan.FromMinutes(20));
        //    client.Client.DefaultRequestHeaders.Accept.Clear();
        //    client.Client.MaxResponseContentBufferSize = 300000;

        //    Dictionary<string, object> req = new Dictionary<string, object>();
        //    req.Add("note[title]", n.Title);
        //    req.Add("note[description]", n.Description);
        //    req.Add("note[image]", image);

        //    string url = string.Format("{0}?token={1}", Api.Note, CurrentSession.CurrentUser.AccessToken);
        //    System.Diagnostics.Debug.WriteLine("create note : " + url + "\nREQ: " + req);
        //    NoteResponse response = await client.FormUpload<NoteResponse>(url, req);
        //    return response;
        //}

        public async Task<WebserviceResponse> DeleteNote (int id)
        {

            string url = string.Format("{0}/{1}?token={2}", Api.Note, id, CurrentSession.CurrentUser.AccessToken);

            System.Diagnostics.Debug.WriteLine("delete URL : " + url);
            WebserviceResponse response = await _client.Delete<WebserviceResponse>(url);
            return response;
        }
    }
}
