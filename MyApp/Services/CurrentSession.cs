﻿using System;
using System.Threading.Tasks;
using MyApp.Models;
using Xamarin.Forms;

namespace MyApp.Services
{
    public class CurrentSession
    {
        private static User _currentUser;
        public static User CurrentUser
        {
            get { return _currentUser ?? (_currentUser = GetCurrentUser()); }
        }

        public static bool LoggedIn
        {
            get
            {
                return CurrentUser != null;
            }
        }

        public static async Task SaveCurrentUser(User u)
        {
            try
            {
                Application.Current.Properties["user_id"] = u.ID;
                Application.Current.Properties["user_email"] = u.Email;
                Application.Current.Properties["user_username"] = u.Username;
                Application.Current.Properties["user_access_token"] = u.AccessToken;
                await Application.Current.SavePropertiesAsync();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error saving user: " + ex.Message);
            }
        }

        public static User GetCurrentUser()
        {
            try
            {
                var user = new User();
                if (Application.Current.Properties.ContainsKey("user_access_token"))
                {
                    user.ID = (int)Application.Current.Properties["user_id"];
                    user.AccessToken = (string)Application.Current.Properties["user_access_token"];
                    user.Email = (string)Application.Current.Properties["user_email"];
                    user.Username = (string)Application.Current.Properties["user_username"];
                    return user;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error getting user: " + ex.Message);
                return null;
            }
        }

        public static void Clear()
        {
            Application.Current.Properties.Remove("user_id");
            Application.Current.Properties.Remove("user_email");
            Application.Current.Properties.Remove("user_access_token");
            Application.Current.Properties.Remove("user_username");
            Application.Current.SavePropertiesAsync();
            _currentUser = null;
        }

    }
}
