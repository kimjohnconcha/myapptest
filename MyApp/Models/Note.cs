﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MyApp.Models
{
    public class Note
    {
        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }


        public string Photo { get; set; }

        public string FullImagePath
        {
            get { return Api.RootUrl + Photo; }
            set { Photo = value; }
        }

        [JsonExtensionData]
        private Dictionary<string, JToken> _additionalData;

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            JToken json = _additionalData["image"];

            if (json.HasValues)
            {
                try
                {
                    Photo = json["url"].ToString();
                }
                catch (Exception e)
                {
                    try
                    {
                        Photo = _additionalData["image"].ToString();
                    }
                    catch (Exception ex)
                    {
                        Photo = "";
                    }
                }
            }
            else
            {
                try
                {
                    Photo = _additionalData["image"].ToString();
                }
                catch (Exception ex)
                {
                    Photo = "";
                }
            }
        }
    }
}
