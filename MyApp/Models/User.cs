﻿using System;
using Newtonsoft.Json;

namespace MyApp.Models
{
    public class User
    {
        [JsonProperty("id")]
        public int ID { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("token")]
        public string AccessToken { get; set; }
    }
}
