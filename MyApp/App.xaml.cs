﻿using System;
using MyApp.Services;
using MyApp.ViewModels;
using MyApp.Views;
using Prism;
using Prism.Ioc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MyApp
{
    public partial class App
    {
        private static WebServiceWrapper _ws;
        public static WebServiceWrapper WS {
            get { return _ws ?? (_ws = new WebServiceWrapper()); }
        }

        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }


        protected async override void OnInitialized()
        {
            //await NavigationService.NavigateAsync("/NavigationPage/NoteItemPage");
            //await NavigationService.NavigateAsync("/NavigationPage/SignupPage");

            if (CurrentSession.LoggedIn)
            {
                await NavigationService.NavigateAsync("/NavigationPage/NoteListPage");
            }
            else
            {
                await NavigationService.NavigateAsync("/NavigationPage/SigninPage");
            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<SigninPage, SigninPageViewModel>();
            containerRegistry.RegisterForNavigation<SignupPage, SignupPageViewModel>();
            containerRegistry.RegisterForNavigation<NoteListPage, NoteListPageViewModel>();
            containerRegistry.RegisterForNavigation<NoteItemPage, NoteItemPageViewModel>();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
