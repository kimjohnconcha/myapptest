﻿using System;
namespace MyApp
{
    public static class Api
    {
        public static string RootUrl = "http://104.200.17.57";

        public static string Version = "/v1";
        public static string Register = RootUrl + Version + "/sign-up";
        public static string Signin = RootUrl + Version + "/sign-in";
        public static string Note = RootUrl + Version + "/notes";
    }
}
