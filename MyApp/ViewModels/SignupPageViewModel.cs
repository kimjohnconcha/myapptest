﻿using System;
using Acr.UserDialogs;
using MyApp.Models;
using MyApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;

namespace MyApp.ViewModels
{
    public class SignupPageViewModel : BindableBase
    {
        INavigationService _navigationService;

        #region Public properties

        private string _email = "";
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private string _username = "";
        public string Username
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _password = "";
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _password_confirmation = "";
        public string PasswordConfirmation
        {
            get { return _password_confirmation; }
            set { SetProperty(ref _password_confirmation, value); }
        }

        #endregion



        public SignupPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            SignupCommand = new DelegateCommand(SignUp);
            GotoSigninPageCommand = new DelegateCommand(GotoSignin);
        }



        #region Commands
        public DelegateCommand SignupCommand { get; private set; }
        public DelegateCommand GotoSigninPageCommand { get; private set; }
        #endregion


        async void SignUp()
        {
            UserDialogs.Instance.ShowLoading();

            try
            {
                if(string.Equals(Password, PasswordConfirmation))
                {
                    if (Password.Length > 5)
                    {
                        var u = new User();
                        u.Email = Email;
                        u.Username = Username;
                        var response = await App.WS.RegisterAsync(u, Password);
                        await CurrentSession.SaveCurrentUser(response.User);
                        await _navigationService.NavigateAsync("/NavigationPage/NoteListPage");
                    }
                    else
                    {
                        await UserDialogs.Instance.AlertAsync("Password too short.");
                    }
                }
                else
                {
                    await UserDialogs.Instance.AlertAsync("Password do not match.");
                }


            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error sign up: " + ex.Message);
            }

            //System.Diagnostics.Debug.WriteLine("Username: " + Username);
            System.Diagnostics.Debug.WriteLine("Email: " + Email);

            UserDialogs.Instance.HideLoading();
        }

        void GotoSignin()
        {
            _navigationService.GoBackAsync();
        }
    }
}
