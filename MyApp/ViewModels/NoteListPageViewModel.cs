﻿using System;
using System.Collections.ObjectModel;
using Acr.UserDialogs;
using MyApp.Models;
using MyApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;

namespace MyApp.ViewModels
{
    public class NoteListPageViewModel : BindableBase, INavigationAware
    {
        INavigationService _navigationService;

        private ObservableCollection<Note> _notes;
        public ObservableCollection<Note> Notes
        {
            get { return _notes; }
            set { SetProperty(ref _notes, value); }
        }

        private string _name = "";
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        public NoteListPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            GotoNoteItemPageCommand = new DelegateCommand(GotoNotePage);
            LogoutCommand = new DelegateCommand(Logout);
            ViewNoteCommand = new DelegateCommand<Note>(ViewNote);
        }

        #region Commands
        public DelegateCommand GotoNoteItemPageCommand { get; private set; }
        public DelegateCommand<Note> ViewNoteCommand { get; private set; }
        public DelegateCommand LogoutCommand { get; private set; }
        #endregion

        async void GotoNotePage()
        {
            await _navigationService.NavigateAsync("NoteItemPage");
        }

        async void Logout()
        {
            try
            {
                var answer = await UserDialogs.Instance.ConfirmAsync("Confirm Logout?", "Log Out", "Log out", "Cancel");

                if (answer)
                {
                    CurrentSession.Clear();
                    await _navigationService.NavigateAsync("/SigninPage");
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error logout: " + ex.Message);
            }
        }

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            //throw new NotImplementedException();

            Notes = new ObservableCollection<Note>();
            GetNotes();
            Name = CurrentSession.CurrentUser.Username;
        }

        public void OnNavigatingTo(INavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }

        async void GetNotes()
        {
            try
            {
                var response = await App.WS.GetNotes(CurrentSession.CurrentUser.AccessToken);
                foreach(var item in response.Notes)
                {
                    Notes.Add(item);
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error getting notes: " + ex.Message);
            }
        }

        void ViewNote(Note note)
        {
            var p = new NavigationParameters();
            p.Add("note", note);

            _navigationService.NavigateAsync("NoteItemPage", p);
        }
    }
}
