﻿using System;
using Acr.UserDialogs;
using MyApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;

namespace MyApp.ViewModels
{
    public class SigninPageViewModel : BindableBase
    {
        INavigationService _navigationService;

        private string _email = "";
        public string Email
        {
            get { return _email; }
            set { SetProperty(ref _email, value); }
        }

        private string _password = "";
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        public SigninPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            GotoSignupCommand = new DelegateCommand(GotoSignUp);
            LoginCommand = new DelegateCommand(Login);
        }

        #region Commands
        public DelegateCommand GotoSignupCommand { get; private set; }
        public DelegateCommand LoginCommand { get; private set; }

        #endregion

        void GotoSignUp()
        {
            _navigationService.NavigateAsync("SignupPage");
        }

        async void Login()
        {
            UserDialogs.Instance.ShowLoading();
            try
            {
                var response = await App.WS.LoginAsync(Email, Password);
                if(response.Success)
                {
                    // save to cache
                    await CurrentSession.SaveCurrentUser(response.User);

                    // navigate to notes list page
                    await _navigationService.NavigateAsync("/NavigationPage/NoteListPage");

                }

            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error login: " + ex.Message);
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }
    }
}
