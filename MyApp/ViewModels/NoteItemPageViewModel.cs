﻿using System;
using System.IO;
using Acr.UserDialogs;
using MyApp.Helpers;
using MyApp.Models;
using Plugin.Media.Abstractions;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;

namespace MyApp.ViewModels
{
    public class NoteItemPageViewModel : BindableBase, INavigationAware
    {
        INavigationService _navigationService;

        private string _photo = "";
        public string Photo
        {
            get { return _photo; }
            set { SetProperty(ref _photo, value); }
        }

        private string _title = "";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private string _desc = "";
        public string Description
        {
            get { return _desc; }
            set { SetProperty(ref _desc, value); }
        }

        public NoteItemPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            UploadPhotoCommand = new DelegateCommand(UploadPhoto);
            SaveCommand = new DelegateCommand(Save);
            DeleteCommand = new DelegateCommand(Delete);
        }

        #region Commands
        public DelegateCommand UploadPhotoCommand { get; private set; }
        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand DeleteCommand { get; private set; }
        #endregion



        protected Stream stream;
        public byte[] photoStream = null;

        async void UploadPhoto()
        {
            try
            {
                MediaFile photo = await MediaHelper.SelectPhotoAsync();

                if(photo != null)
                {
                    stream = photo.GetStream();
                    photoStream = new byte[stream.Length];
                    stream.ToByteArray().CopyTo(photoStream, 0);


                    System.Diagnostics.Debug.WriteLine("Photo path: " + photo.Path);
                    Photo = photo.Path;
                    RaisePropertyChanged("Photo");
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error photo upload: " + ex.Message);
            }
        }

        async void Save()
        {
            UserDialogs.Instance.ShowLoading();

            try
            {
                var n = new Note();
                n.ID = Note.ID;
                n.Title = Title;
                n.Description = Description;

                //if (Note.ID == 0)
                //{
                    var response = await App.WS.CreateNote(n, photoStream);
                    if(response.Success)
                    {
                        await UserDialogs.Instance.AlertAsync("Successfully created a note.");
                        await _navigationService.GoBackAsync();
                    }
                    else
                    {
                        await UserDialogs.Instance.AlertAsync("Error creating a note.");
                    }
                //}
                //else
                //{
                    //var response = await App.WS.CreateNote(n, photoStream);
                    //if (response.Success)
                    //{
                    //    await UserDialogs.Instance.AlertAsync("Successfully created a note.");
                    //    await _navigationService.GoBackAsync();
                    //}
                    //else
                    //{
                    //    await UserDialogs.Instance.AlertAsync("Error creating a note.");
                    //}
                //}
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error save: " + ex.Message);
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        async void Delete()
        {
            UserDialogs.Instance.ShowLoading();
            try
            {
                if (Note.ID != 0)
                {
                    var answer = await UserDialogs.Instance.ConfirmAsync("Are you sure you want to delete this note?","Delete Note","Delete","Cancel");
                
                    if(answer)
                    {
                        var response = await App.WS.DeleteNote(Note.ID);
                        if (response.Success)
                        {
                            await UserDialogs.Instance.AlertAsync("Successfully deleted a note.");
                            await _navigationService.GoBackAsync();
                        }
                        else
                        {
                            await UserDialogs.Instance.AlertAsync("Error deleting a note.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error delete: " + ex.Message);
            }
            finally
            {
                UserDialogs.Instance.HideLoading();
            }
        }

        private Note Note = new Note();

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("note"))
            {
                Note = (Note)parameters["note"];
                Title = Note.Title;
                Description = Note.Description;
                Photo = Note.FullImagePath;
            }
        }

        public void OnNavigatingTo(INavigationParameters parameters)
        {
            //throw new NotImplementedException();
        }
    }
}
