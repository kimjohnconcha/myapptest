﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MyApp.Views
{
    public partial class SigninPage : ContentPage
    {
        public SigninPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
