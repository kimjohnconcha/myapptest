﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace MyApp.Views
{
    public partial class SignupPage : ContentPage
    {
        public SignupPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
